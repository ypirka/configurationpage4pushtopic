/**
 * Created by Administrator on 27.12.2018.
 */
({
    getSupportedObjectsHelper: function (component, event, helper) {
        var action = component.get("c.getSupportedSObject");
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.listAvailableSObjects", response.getReturnValue());
            } else {
                console.error('Callback getSupportedObjectsHelper failed');
            }
        });
        $A.enqueueAction(action);
    },
    getSObjectPushtopicMapping: function (component, event, helper) {
        var action = component.get("c.getSObjectAndPushtopicMapping");
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.mapObjectByListPT", response.getReturnValue());
                // component.set('v.selectedObject',null);
            } else {
                console.error('Callback getSObjectPushtopicMapping failed');
            }
        });
        $A.enqueueAction(action);
    },
    getTemplatePushTopic: function (component, event, helper) {
        var newPTMappingRecord = {
            'sobjectType': 'PushTopic',
            'Name': '',
            'Query': '',
            'ApiVersion': '',
            'IsActive': false,
            'NotifyForFields': '',
            'NotifyForOperationCreate': false,
            'NotifyForOperationDelete': false,
            'NotifyForOperationUndelete': false,
            'NotifyForOperationUpdate': false,
            'Description': '',

        };
        component.set('v.newPushTopic', newPTMappingRecord);
    },
    showToast: function (type, message, title) {
        var toastEvent = $A.get('e.force:showToast');
        if (toastEvent) {
            var toastParams = {
                // type - error, warning, success, or info
                'type': type,
                'message': message
            };
            if (title)
                toastParams.title = title;
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert(message);
        }
    }
})