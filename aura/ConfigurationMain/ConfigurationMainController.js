/**
 * Created by Administrator on 27.12.2018.
 */
({
    doInit: function (component, event, helper) {
        helper.getSupportedObjectsHelper(component, event, helper);
        helper.getSObjectPushtopicMapping(component, event, helper);
    },
    onChangeSelector: function (component, event, helper) {
        var selectedObj = component.find('selectSObject').get('v.value');
        component.set('v.selectedObject', selectedObj);
    },
    addPushTopic: function (component, event, helper) {
        helper.getTemplatePushTopic(component, event, helper);

        var newPushTopicButton = component.find("push-topic-new-trigger");
        $A.util.addClass(newPushTopicButton, "slds-hide");
        var newPushTopicForm = component.find("push-topic-new-form");
        $A.util.removeClass(newPushTopicForm, "slds-hide");
    },
    saveNewPushTopic: function (component, event, helper) {
        var newPushTopic = component.get('v.newPushTopic');
        var action = component.get("c.savePushTopic");
        action.setParams({
            'topic': JSON.stringify(newPushTopic),
            'objectPushtopicMapping': component.get('v.mapObjectByListPT')
        });
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                component.set('v.mapObjectByListPT', response.getReturnValue());
                var newPushTopicButton = component.find("push-topic-new-trigger");
                $A.util.removeClass(newPushTopicButton, "slds-hide");
                var newPushTopicForm = component.find("push-topic-new-form");
                $A.util.addClass(newPushTopicForm, "slds-hide");
                helper.showToast('success', 'Changes were saved successfully!');

            } else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    helper.showToast('error', errors[0].message);
                }
                else {
                    helper.showToast('error', 'Unknown error');
                }
                console.error('Problem saving record, error: ' + JSON.stringify(response.getError()[0].message));
            }
        });
        $A.enqueueAction(action);
    },
    cancel: function (component, event, helper) {
        var newPushTopicButton = component.find("push-topic-new-trigger");
        $A.util.removeClass(newPushTopicButton, "slds-hide");
        var newPushTopicForm = component.find("push-topic-new-form");
        $A.util.addClass(newPushTopicForm, "slds-hide");
    }
})