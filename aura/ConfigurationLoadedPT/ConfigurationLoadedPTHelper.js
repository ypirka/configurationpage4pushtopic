/**
 * Created by Administrator on 28.12.2018.
 */
({
    showToast: function (type, message, title) {
        var toastEvent = $A.get('e.force:showToast');
        if (toastEvent) {
            var toastParams = {
                // type - error, warning, success, or info
                'type': type,
                'message': message
            };
            if (title)
                toastParams.title = title;
            toastEvent.setParams(toastParams);
            toastEvent.fire();
        } else {
            alert(message);
        }
    }
})