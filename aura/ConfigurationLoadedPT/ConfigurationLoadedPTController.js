/**
 * Created by Administrator on 28.12.2018.
 */
({
    editPushTopic: function (component, event, helper) {
        var editPushTopicForm = component.find("push-topic-edit-form");
        $A.util.removeClass(editPushTopicForm, "slds-hide");
        var previewPushTopicForm = component.find("push-topic-preview-form");
        $A.util.addClass(previewPushTopicForm, "slds-hide");
    },
    cancelEditPushTopic : function(component, event, helper) {
        // update ui
        var editPushTopicForm = component.find("push-topic-edit-form");
        $A.util.addClass(editPushTopicForm, "slds-hide");
        var previewPushTopicForm = component.find("push-topic-preview-form");
        $A.util.removeClass(previewPushTopicForm, "slds-hide");
    },
    handleSaveRecord: function(component, event, helper) {
        var simpleRecord = component.get('v.simpleRecord');
        var action = component.get("c.updatePushTopic");
        action.setParams({
            'topic': JSON.stringify(simpleRecord)
        });
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                helper.showToast('success', 'Changes were saved successfully!');
                var editPushTopicForm = component.find("push-topic-edit-form");
                $A.util.addClass(editPushTopicForm, "slds-hide");
                var previewPushTopicForm = component.find("push-topic-preview-form");
                $A.util.removeClass(previewPushTopicForm, "slds-hide");
            } else {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    helper.showToast('error', errors[0].message);
                }
                else {
                    helper.showToast('error', 'Unknown error');
                }
                component.set('v.recordError', 'Problem updating record, error: ' + JSON.stringify(response.getError()[0].message));
            }
        });
        $A.enqueueAction(action);
    }
})