/**
 * Created by Administrator on 27.12.2018.
 */
({
    handleChangeSObj: function (component, event, helper) {
        var mapObjectByListPT = component.get("v.mapObjectByListPT");
        var selectedObj = component.get('v.selectedObject');
        if(!!mapObjectByListPT && !!selectedObj) {
            component.set('v.listPushTopicByObject', mapObjectByListPT[selectedObj]);
        }
        component.set('v.selectedPushTopic',null);
    },
    onChangePTSelector: function (component, event, helper) {
        var selectedPTId = component.find('selectPT').get('v.value');
        if(!selectedPTId) {
            component.set('v.selectedPushTopic',null);
            return;
        }
        var listPushTopicByObject = component.get('v.listPushTopicByObject');
        listPushTopicByObject.forEach(function (item, i, arr) {
            if(item.Id === selectedPTId) {
                component.set('v.selectedPushTopic',item);
            }
        })
    }
})