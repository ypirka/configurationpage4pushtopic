public class FillInOrgByPushTopics {
    static Integer start = 0;
    static Integer finish = 3;
    public static void generatePT() {
        if (Test.isRunningTest()) {
			start = 4;
            finish = 7;
        }
        List<PushTopic> list1 = new List<PushTopic>();
        for (Integer i = start; i < finish; i++) {
            PushTopic topic = new PushTopic();
            topic.ApiVersion = 43;
            topic.Name = 'Account'+i;
            topic.Query = 'SELECT Id , Name FROM Account';
            topic.NotifyForFields = 'All';
            topic.NotifyForOperationCreate = true;
            topic.NotifyForOperationUpdate = false;
            topic.Description = 'lorem ipsum'+i;
            list1.add(topic);
        }
        insert list1;
        
        List<PushTopic> list2 = new List<PushTopic>();
        for (Integer i = start; i < finish; i++) {
            PushTopic topic = new PushTopic();
            topic.ApiVersion = 43;
            topic.Name = 'Contact'+i;
            topic.Query = 'SELECT Id , Phone FROM Contact';
            topic.NotifyForFields = 'All';
            topic.NotifyForOperationCreate = true;
            topic.NotifyForOperationUpdate = true;
            topic.NotifyForOperationDelete = true;
            topic.Description = 'lorem ipsum'+i;
            list2.add(topic);
        }
        insert list2;
    }
}