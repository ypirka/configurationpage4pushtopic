/**
 * Created by Administrator on 27.12.2018.
 */

public with sharing class ConfigurationMainController {

    @AuraEnabled
    public static List <String> getSupportedSObject() {
        String[] supportedStandardObj = new String[]{
                'Account',
                'Campaign',
                'Case',
                'Contact',
                'ContractLineItem',
                'Entitlement',
                'Lead',
                'LiveChatTranscript',
                'Opportunity',
                'Quote',
                'QuoteLineItem',
                'ServiceAppointment',
                'ServiceContract',
                'Task',
                'WorkOrder',
                'WorkOrderLineItem'
        };
        List <String> allOptions = new list <String>(supportedStandardObj);
        Map<String, SObjectType> sObjects = Schema.getGlobalDescribe();
        for (String apiName : sObjects.keySet()) {
            if (sObjects.get(apiName).getDescribe().isCustom()) {
                // Metadata was excluded in case I supposed this type wasn't necessary
                if (!sObjects.get(apiName).getDescribe().name.endsWith('__mdt')) {
                    allOptions.add(sObjects.get(apiName).getDescribe().name);
                }
            }
        }
        allOptions.sort();
        return allOptions;
    }

    @AuraEnabled
    public static Map<String, List<PushTopic>> getSObjectAndPushtopicMapping() {

        List<PushTopic> pushTopics = [
                SELECT Id, Name, ApiVersion, Description, IsActive, NotifyForFields,
                        NotifyForOperationCreate, NotifyForOperationDelete, NotifyForOperationUndelete,
                        NotifyForOperationUpdate, Query
                FROM PushTopic
        ];
        Map<String, List<PushTopic>> objectPushtopicMapping = new Map<String, List<PushTopic>>();
        for (PushTopic topic : pushTopics) {


            String query = topic.Query;
            Pattern p = Pattern.compile('FROM (\\w+).*');
            Matcher m = p.matcher(query);
            if (m.find()) {
                if (objectPushtopicMapping.containsKey(m.group(1))) {
                    List<PushTopic> tempPushTopicsList = objectPushtopicMapping.get(m.group(1));
                    tempPushTopicsList.add(topic);
                    objectPushtopicMapping.put(m.group(1), tempPushTopicsList);
                } else {
                    objectPushtopicMapping.put(m.group(1), new List<PushTopic>{
                            topic
                    });
                }
            }
        }
        return objectPushtopicMapping;
    }

    @AuraEnabled
    public static void updatePushTopic(String topic) {
        try {
            PushTopic p = (PushTopic) JSON.deserialize(topic, PushTopic.class);
            update p;
        } catch (Exception e) {
            AuraHandledException err = new AuraHandledException('Save error: ' + e.getMessage());
            err.setMessage('DG Language Mappings save error: ' + e.getMessage());
            throw err;
        }
    }

    @AuraEnabled
    public static Map<String, List<PushTopic>> savePushTopic(String topic, Map<String, List<PushTopic>> objectPushtopicMapping) {
        System.debug('objectPushtopicMapping ' + objectPushtopicMapping);
        try {
            PushTopic pushTopic = (PushTopic) JSON.deserialize(topic, PushTopic.class);
            if (String.isBlank(pushTopic.NotifyForFields)) {
                pushTopic.NotifyForFields = 'Referenced ';
            }
            insert pushTopic;
            String query = pushTopic.Query;
            Pattern p = Pattern.compile('FROM (\\w+).*');
            Matcher m = p.matcher(query);
            if (m.find()) {
                if (objectPushtopicMapping.containsKey(m.group(1))) {
                    List<PushTopic> tempPushTopicsList = objectPushtopicMapping.get(m.group(1));
                    tempPushTopicsList.add(pushTopic);
                    objectPushtopicMapping.put(m.group(1), tempPushTopicsList);
                } else {
                    objectPushtopicMapping.put(m.group(1), new List<PushTopic>{
                            pushTopic
                    });
                }
            }
            return objectPushtopicMapping;
        } catch (Exception e) {
            AuraHandledException err = new AuraHandledException('Save error: ' + e.getMessage());
            err.setMessage('DG Language Mappings save error: ' + e.getMessage());
            throw err;
        }
    }
}