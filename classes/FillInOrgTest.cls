@isTest
public with sharing class FillInOrgTest {
	@isTest
    static void testGeneratePT() {
        
        Test.startTest();
        FillInOrgByPushTopics.generatePT();
        Test.stopTest();
        
        List<PushTopic> topics = [SELECT Id FROM PushTopic];
        System.assertEquals(6, topics.size());
    }
}