/**
 * Created by Administrator on 28.12.2018.
 */
@isTest
public with sharing class ConfigurationMainControllerTest {


    static void generateData() {

        PushTopic topic = new PushTopic();
        topic.ApiVersion = 43;
        topic.Name = 'Account.test';
        topic.Query = 'SELECT Id , Name FROM Account';
        topic.NotifyForFields = 'All';
        topic.NotifyForOperationCreate = true;
        topic.NotifyForOperationUpdate = false;
        topic.Description = 'Test lorem ipsum';
        insert topic;

        PushTopic topic2 = new PushTopic();
        topic2.ApiVersion = 43;
        topic2.Name = 'Contact.test';
        topic2.Query = 'SELECT Id , Phone FROM Contact';
        topic2.NotifyForFields = 'All';
        topic2.NotifyForOperationCreate = true;
        topic2.NotifyForOperationUpdate = false;
        topic2.Description = 'Test lorem ipsum';
        insert topic2;

        PushTopic topic3 = new PushTopic();
        topic3.ApiVersion = 43;
        topic3.Name = 'Contact.test2';
        topic3.Query = 'SELECT Id , Phone FROM Contact';
        topic3.NotifyForFields = 'All';
        topic3.NotifyForOperationCreate = true;
        topic3.NotifyForOperationUpdate = false;
        topic3.Description = 'Test lorem ipsum2';
        insert topic3;
    }

    @isTest
    static void test_getSupportedSObject() {
        String[] supportedStandardObj = new String[]{
                'Account',
                'Campaign',
                'Case',
                'Contact',
                'ContractLineItem',
                'Entitlement',
                'Lead',
                'LiveChatTranscript',
                'Opportunity',
                'Quote',
                'QuoteLineItem',
                'ServiceAppointment',
                'ServiceContract',
                'Task',
                'WorkOrder',
                'WorkOrderLineItem'
        };
        List <String> allOptions = new list <String>(supportedStandardObj);
        Map<String, SObjectType> sObjects = Schema.getGlobalDescribe();
        for (String apiName : sObjects.keySet()) {
            if (sObjects.get(apiName).getDescribe().isCustom()) {
                // Metadata was excluded in case I supposed this type wasn't necessary
                if (!sObjects.get(apiName).getDescribe().name.endsWith('__mdt')) {
                    allOptions.add(sObjects.get(apiName).getDescribe().name);
                }
            }
        }
        Test.startTest();
        List<String> listTest = ConfigurationMainController.getSupportedSObject();
        Test.stopTest();
        System.assertNotEquals(null, listTest);
        System.assertEquals(allOptions.size(), listTest.size());
    }

    @isTest
    static void test_getSObjectAndPushtopicMapping() {

        generateData();
        Test.startTest();
        Map<String, List<PushTopic>> mapping = ConfigurationMainController.getSObjectAndPushtopicMapping();
        Test.stopTest();

        System.assertEquals(2, mapping.size());
        System.assertEquals(true, mapping.containsKey('Contact'));
        System.assertEquals(2, mapping.get('Contact').size());
        System.assertEquals(true, mapping.containsKey('Account'));

    }

    @isTest
    static void test_updatePushTopic() {

        generateData();
        PushTopic pushTopic = [SELECT Id, Name, ApiVersion, Description, IsActive, NotifyForFields,
        NotifyForOperationCreate, NotifyForOperationDelete, NotifyForOperationUndelete,
        NotifyForOperationUpdate, Query FROM PushTopic LIMIT 1];
        pushTopic.Name = 'Test';

        Test.startTest();
        ConfigurationMainController.updatePushTopic(JSON.serialize(pushTopic));
        Test.stopTest();

        PushTopic selectedTopic = [SELECT Id, Name FROM PushTopic WHERE Id=:pushTopic.Id  LIMIT 1];
        System.assertEquals('Test', selectedTopic.Name);

    }

    @isTest
    static void test_savePushTopic() {

        Map<String, List<PushTopic>> objectPushtopicMapping = new Map<String, List<PushTopic>>();
        PushTopic topic = new PushTopic();
        topic.ApiVersion = 43;
        topic.Name = 'Account.test';
        topic.Query = 'SELECT Id , Name FROM Account';
        topic.NotifyForFields = 'All';
        topic.NotifyForOperationCreate = true;
        topic.NotifyForOperationUpdate = false;
        topic.Description = 'Test lorem ipsum';
        insert topic;

        PushTopic topic2 = new PushTopic();
        topic2.ApiVersion = 43;
        topic2.Name = 'Contact.test';
        topic2.Query = 'SELECT Id , Phone FROM Contact';
        topic2.NotifyForFields = 'All';
        topic2.NotifyForOperationCreate = true;
        topic2.NotifyForOperationUpdate = false;
        topic2.Description = 'Test lorem ipsum';

        PushTopic topic3 = new PushTopic();
        topic3.ApiVersion = 43;
        topic3.Name = 'Contact.test2';
        topic3.Query = 'SELECT Id , Phone FROM Contact';
        topic3.NotifyForFields = 'All';
        topic3.NotifyForOperationCreate = true;
        topic3.NotifyForOperationUpdate = false;
        topic3.Description = 'Test lorem ipsum2';
        objectPushtopicMapping.put('Account', new List<PushTopic>{topic});

        Test.startTest();
        Map<String, List<PushTopic>> mapping1 = ConfigurationMainController.savePushTopic(JSON.serialize(topic2), objectPushtopicMapping);
        Map<String, List<PushTopic>> mapping2 = ConfigurationMainController.savePushTopic(JSON.serialize(topic3), objectPushtopicMapping);
        Test.stopTest();

        System.assertEquals(2, mapping1.size());
        System.assertEquals(2, mapping1.get('Contact').size());
        System.assertEquals(1, mapping1.get('Account').size());
        System.assertEquals(2, mapping2.get('Contact').size());
        System.assertEquals(1, mapping2.get('Account').size());
    }

}